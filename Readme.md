# Windows 10 fixes to apply
## Disable connectivity check on windows
https://networkproguide.com/fix-connect-attempts-to-www-msftconnecttest-com-windows-server-2016/

## Switch NTP server
net time /setsntp:pool.ntp.org
w32tm /config /manualpeerlist:"fr.pool.ntp.org" /syncfromflags:manual /reliable:yes /update 

# Things that can be broken
- Internet connectivity
- Opera speeddials

# Things to do
- add support to regex (they are ommited from the file, currently requires to add them manually in the interface)
- block tiktok : https://raw.githubusercontent.com/llacb47/mischosts/master/social/tiktok-regex.list
